# -*- coding: utf-8 -*-

"""
Faça um programa que peça dois numeros inteiros.
Imprima a soma desses dois numeros na tela.
"""

print("="*60)

a = int(input("Digite um numero: "))
b = int(input("Digite um numero: "))

print("A soma dos numeros: ", (a + b))

print("="*60)